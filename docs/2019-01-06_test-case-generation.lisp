(let ((assert-statements (loop for test-num in (list (1+ (- (expt 2 63))) (1- (expt 2 63)))
                               collect (format nil "Assert (Ordinal (~A) = \"~:*~:R\", \"Ordinal failed on ~:*~A\");" test-num))))
  (mapcar #'length
          (mapc #'write-line assert-statements)))
