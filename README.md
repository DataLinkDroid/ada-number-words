# Ada 2012 Number Words Library
Convert whole numbers into an equivalent English language form.

This Ada 2012 library with C language interface, provides routines to
convert an integer number into English words, in either cardinal or
ordinal form. The style used is the US English one, as per the
following examples:

### Cardinals

 * 999   = nine hundred ninety-nine
 * 1000  = one thousand
 * 1001  = one thousand one

### Ordinals

 * 100th = one hundredth
 * 101st = one hundred first
 * 202nd = two hundred second


## Building

Using the included Makefile, you can build the static, static position
independent code (PIC), and the dynamic shared library, simply by
typing:

```bash
$ make
```

To build a dynamic shared library suitable for calling from C, use:

```bash
$ make dynamic_encapsulated
```

You can also build directly with gprbuild, as per the following examples:

```bash
$ gprbuild -f -P number_words.gpr -XLib_Kind=Dynamic
$ gprbuild -f -P number_words.gpr -XLib_Kind=Dynamic_Encapsulated
$ gprbuild -f -P number_words.gpr -XLib_Kind=Static
$ gprbuild -f -P number_words.gpr -XLib_Kind=Static_PIC
$ gprbuild -f -P number_words.gpr -XLib_Kind=Static_PIC_Encapsulated
```

## Testing

The easiest way to build and run the test cases is to use the included
Makefile, and type on the command line:

```bash
$ make test
```

Alternatively, in the top-level directory:

```bash
$ cd src/gnattest/harness
$ gprbuild -P test_number_words.gpr -XLib_Kind=Static_PIC
$ ./test_runner
```

Test output should be something like:

```
english_cardinals_ordinals.ads:52:4: info: corresponding test PASSED
english_cardinals_ordinals.ads:60:4: info: corresponding test PASSED
2 tests run: 2 passed; 0 failed; 0 crashed.
```

## Building and Running the C Example Program

From the top-level directory, after having built the Dynamic_Encapsulated library target:

```bash
$ gprbuild -d -Pc_example.gpr
$ LD_LIBRARY_PATH=lib ./example_c
```



