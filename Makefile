# Build number words library


all : static static_pic dynamic

static : 
	gprbuild -XLib_Kind=Static -Pnumber_words.gpr

static_pic : 
	gprbuild -XLib_Kind=Static_PIC -Pnumber_words.gpr

dynamic : 
	gprbuild -f -XLib_Kind=Dynamic -Pnumber_words.gpr

dynamic_encapsulated : 
	gprbuild -f -XLib_Kind=Dynamic_Encapsulated -Pnumber_words.gpr

buildtest :
	gprbuild -f -XLib_Kind=Static_PIC -P./src/gnattest/harness/test_number_words.gpr

test : buildtest
	./src/gnattest/harness/test_runner

clean :
	gprclean -r -XLib_Kind=Static -Pnumber_words
	gprclean -r -XLib_Kind=Static -P./src/gnattest/harness/test_number_words.gpr
	gprclean -r -XLib_Kind=Static_PIC -Pnumber_words
	gprclean -r -XLib_Kind=Static_PIC -P./src/gnattest/harness/test_number_words.gpr
	gprclean -r -XLib_Kind=Dynamic -Pnumber_words
	gprclean -r -XLib_Kind=Dynamic_Encapsulated -Pnumber_words




# Local Variables:
# eval:(ada-parse-prj-file "number_words.prj")
# eval:(ada-select-prj-file "number_words.prj")
# End:
