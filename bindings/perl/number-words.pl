#!/usr/bin/perl

use strict;

package Number_Words;

use Inline C => Config =>
           ENABLE => AUTOWRAP =>
           LIBS => '-lnumberwords';

use Inline C => <<'END_OF_C';
void init_number_words (void);
void finalize_number_words (void);
char* card_words (long long);
char* ord_words (long long);
END_OF_C

package main;

Number_Words::init_number_words();
my $string = Number_Words::card_words (12);
print $string;
Number_Words::finalize_number_words();

