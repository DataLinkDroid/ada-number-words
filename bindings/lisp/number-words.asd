;;;; number-words.asd

(asdf:defsystem #:number-words
  :description "Binding to Ada Number Words library."
  :author "David Trudgett <David.Trudgett@eclecticse.com.au>"
  :license  "GNU LGPL 2.1"
  :version "0.0.1"
  :serial t
  :components ((:file "package")
               (:file "specials")
               (:file "foreign-libs")
               (:file "foreign-functions")
               (:file "number-words"))
  :depends-on ("cffi"))
