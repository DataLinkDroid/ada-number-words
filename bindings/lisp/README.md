# number-words
### _David Trudgett: <David .Trudgett @ eclecticse .com .au>_ (remove spaces)

This package demonstrates how to load the Ada Number Words library and
exercise its functions.

At present this is not quite working for SBCL under Ubuntu GNU/Linux,
for reasons that are not understood. A solution is currently being
worked on.

## License

The licence for this Lisp package is the LLGPL 2.1 (Lisp Library
General Public Licence). This is the same licence as used for the Ada
source, but with required clarifications for use with Lisp. The
clarifications are contained in Franz Inc.'s Preamble to the LGPL, as
found here:

http://opensource.franz.com/preamble.html




