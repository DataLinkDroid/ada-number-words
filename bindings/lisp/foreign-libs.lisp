;;;; foreign-libs.lisp

(in-package #:number-words)

(define-foreign-library number-words
  (:unix (:or "libnumberwords.so.1.1"
              "libnumberwords.so.1"
              "libnumberwords.so"))
  (t (:default "libnumberwords")))

