;;;; foreign-functions.lisp

(in-package #:number-words)

(load-foreign-library 'number-words)
;;(use-foreign-library number-words)

(defcfun (init-number-words "init_number_words"
                            :library number-words
                            :convention :cdecl)
    :void)

(defcfun (finalise-number-words "finalize_number_words"
                                :library number-words
                                :convention :cdecl)
    :void)

(defcfun (c-cardinal "card_words" :library number-words :convention :cdecl) :pointer
  "Return the English language cardinal form equivalent in words of the given number."
  (number :int64))

(defcfun (c-ordinal "ord_words" :library number-words :convention :cdecl) :pointer
  "Return the English language ordinal equivalent in words of the given number."
  (number :int64))

