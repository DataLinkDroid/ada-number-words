    --  Number Word Library - Convert a number to English words.
    --  Copyright (C) 2017-2019 David K. Trudgett

    --  This library is free software; you can redistribute it and/or
    --  modify it under the terms of the GNU Lesser General Public
    --  License as published by the Free Software Foundation; either
    --  version 2.1 of the License, or (at your option) any later
    --  version.

    --  This library is distributed in the hope that it will be
    --  useful, but WITHOUT ANY WARRANTY; without even the implied
    --  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
    --  PURPOSE. See the GNU Lesser General Public License for more
    --  details.

    --  You should have received a copy of the GNU Lesser General
    --  Public License along with this library; if not, write to the
    --  Free Software Foundation, Inc., 51 Franklin Street, Fifth
    --  Floor, Boston, MA 02110-1301 USA


-- @summary
-- Convert whole numbers into an equivalent English language form.
--
-- @description
-- This package provides routines to convert an integer number into
-- English words, in either cardinal or ordinal form. The style used
-- is the US English one, as per the following examples:
--
-- Cardinals:
--
-- * 999   = nine hundred ninety-nine
-- * 1000  = one thousand
-- * 1001  = one thousand one
--
-- Ordinals:
--
-- * 100th = one hundredth
-- * 101st = one hundred first
-- * 202nd = two hundred second
--
package English_Cardinals_Ordinals is

   Number_Too_Large : exception;
   -- Raised if the given number is too large to be converted into
   -- English words by this package. Will never be raised for any
   -- 64-bit signed integer quantity.

   type Language_Variant is (US_English, UK_English);
   -- Type of English rendering to use. Only US English is currently supported.

   type English_Number is range -2 ** 63 + 1 .. 2 ** 63 - 1;
   -- We limit acceptable inputs to 64-bit signed integers, excluding -2**63.


   function Cardinal (Number  : English_Number;
                      Variant : Language_Variant := US_English) return String;
   -- Return the equivalent number in English words using cardinal form.
   -- @param Number An integer number in the range accepted for converting to English words.
   -- @param Variant Select either US or UK language variant. Only US currently supported.
   -- @return String of English words representing the given cardinal number.


   function Ordinal (Number  : English_Number;
                     Variant : Language_Variant := US_English) return String;
   -- Return the equivalent number in English words using ordinal form.
   -- @param Number An integer number in the range accepted for converting to English words.
   -- @param Variant Select either US or UK language variant. Only US currently supported.
   -- @return String of English words representing the given ordinal number.


end English_Cardinals_Ordinals;
