#include <stdlib.h>
#include <stdio.h>
#include "numberwords.h"

int main (int argc, char **argv) {
  
  long testValues[21] = 
  {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 20, 30, 42, 100, 1000, 1000000, 1000000000, 1000000000000};

 init_number_words();
  
  for (int i = 0; i < 21; i++) {
    char* myCardinal = card_words(testValues[i]);
    printf("%s\n", myCardinal);
    free(myCardinal);
  }

  finalize_number_words();
  
  return 0;
}
