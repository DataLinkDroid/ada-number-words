with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C.Extensions; use Interfaces.C.Extensions;

package English_Cardinals_Ordinals.CFFI is
   
   procedure Initialize_API
     with Export, Convention => C, Link_Name => "init_number_words";
   
   procedure Finalize_API
     with Export, Convention => C, Link_Name => "finalize_number_words";

   function Cardinal_Words (Number : long_long) return Chars_Ptr
     with Export, Convention => C, Link_Name => "card_words"; 
   
   function Ordinal_Words (Number : long_long) return Chars_Ptr
     with Export, Convention => C, Link_Name => "ord_words";

end English_Cardinals_Ordinals.CFFI;
