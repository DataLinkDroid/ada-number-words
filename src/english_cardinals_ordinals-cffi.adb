with Interfaces.C.Strings; use Interfaces.C.Strings;
with Interfaces.C.Extensions;

package body English_Cardinals_Ordinals.CFFI is

   procedure Initialize_API is
      procedure Numberwordsinit
        with Import, Convention => C;
   begin
      Numberwordsinit;
   end Initialize_API;


   procedure Finalize_API is
      procedure Numberwordsfinal
        with Import, Convention => C;
   begin
      Numberwordsfinal;
   end Finalize_API;

   --------------
   -- Cardinal --
   --------------

   function Cardinal_Words
     (Number : Interfaces.C.Extensions.long_long)
      return Interfaces.C.Strings.chars_ptr
   is
     (New_String (English_Cardinals_Ordinals.Cardinal (English_Number (Number))));

   -------------
   -- Ordinal --
   -------------

   function Ordinal_Words
     (Number : Interfaces.C.Extensions.long_long)
      return Interfaces.C.Strings.chars_ptr
   is
     (New_String (English_Cardinals_Ordinals.Ordinal (English_Number (Number))));

end English_Cardinals_Ordinals.CFFI;
