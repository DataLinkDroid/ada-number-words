#ifndef NUMBERWORDS
#define NUMBERWORDS
#include <inttypes.h>
extern void init_number_words (void);
extern void finalize_number_words (void);
extern char* card_words (int64_t number);
extern char* ord_words (int64_t number);
#endif
