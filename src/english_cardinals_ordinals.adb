    --  Number Word Library - Convert a number to English words.
    --  Copyright (C) 2017-2019  David K. Trudgett

    --  This library is free software; you can redistribute it and/or
    --  modify it under the terms of the GNU Lesser General Public
    --  License as published by the Free Software Foundation; either
    --  version 2.1 of the License, or (at your option) any later version.

    --  This library is distributed in the hope that it will be useful,
    --  but WITHOUT ANY WARRANTY; without even the implied warranty of
    --  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    --  Lesser General Public License for more details.

    --  You should have received a copy of the GNU Lesser General Public
    --  License along with this library; if not, write to the Free Software
    --  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
    --  USA


with Ada.Strings; use Ada.Strings;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package body English_Cardinals_Ordinals is

   subtype Small_English_Number is English_Number range 0 .. 999;

   --  Only Ada 2012 feature used?
   --  function "+" (S : String) return Unbounded_String is
   --     (To_Unbounded_String (S));

   function "+" (Right : String) return Unbounded_String
     renames To_Unbounded_String;


   Cardinal_Ones : constant array (1 .. 9) of Unbounded_String :=
                     (+"one", +"two", +"three", +"four", +"five",
                      +"six", +"seven", +"eight", +"nine");

   Cardinal_Tens : constant array (1 .. 9) of Unbounded_String :=
                     (+"ten", +"twenty", +"thirty", +"forty", +"fifty",
                      +"sixty", +"seventy", +"eighty", +"ninety");

   Cardinal_Teens : constant array (1 .. 9) of Unbounded_String :=
                      (+"eleven", +"twelve", +"thirteen", +"fourteen",
                       +"fifteen", +"sixteen", +"seventeen", +"eighteen", +"nineteen");

   Cardinal_Periods : constant array (1 .. 21) of Unbounded_String :=
                        (+" thousand", +" million", +" billion", +" trillion", +" quadrillion",
                         +" quintillion", +" sextillion", +" septillion", +" octillion", +" nonillion",
                         +" decillion", +" undecillion", +" duodecillion", +" tredecillion",
                         +" quattuordecillion", +" quindecillion", +" sexdecillion", +" septendecillion",
                         +" octodecillion", +" novemdecillion", +" vigintillion");

   Ordinal_Ones  : constant array (1 .. 9) of Unbounded_String :=
                     (+"first", +"second", +"third", +"fourth",
                      +"fifth", +"sixth", +"seventh", +"eighth", +"ninth");

   Ordinal_Tens  : constant array (1 .. 9) of Unbounded_String :=
                     (+"tenth", +"twentieth", +"thirtieth", +"fortieth",
                      +"fiftieth", +"sixtieth", +"seventieth", +"eightieth", +"ninetieth");


   procedure Small_Cardinal
     (English_Words : in out Unbounded_String;
      Small_Number : Small_English_Number)
   is
      Hundreds : Integer := Integer(Small_Number) / 100;
      Remain   : Integer := Integer(Small_Number) mod 100;
   begin
      if Hundreds > 0 then
         Append (English_Words, Cardinal_Ones (Hundreds) & " hundred");
         if Remain > 0 then
            Append (English_Words, Space);
         end if;
      end if;
      if Remain > 0 then
         declare
            Tens : Integer := Remain / 10;
            Ones : Integer := Remain mod 10;
         begin
            if Tens > 1 then
               Append (English_Words, Cardinal_Tens (Tens));
               if Ones > 0 then
                  Append (English_Words, "-" & Cardinal_Ones (Ones));
               end if;
            elsif Tens = 1 and then Ones = 0 then
               Append (English_Words, Cardinal_Tens (Tens));
            elsif Tens = 1 and then Ones > 0 then
               Append (English_Words, Cardinal_Teens (Ones));
            elsif Ones > 0 then
               Append (English_Words, Cardinal_Ones (Ones));
            end if;
         end;
      end if;
   end;


   procedure Cardinal_Aux
     (Result : in out Unbounded_String;
      Number : in English_Number;
      Period : in Natural;
      Err    : in English_Number)
   is
      Beyond : English_Number := Number / 1000;
      Here   : Small_English_Number := Number mod 1000;
   begin
      if Period > 21 then
         -- Using 64-bit signed integers, this should never happen.
         -- If larger integers are used in the future, this could then become an issue.
         raise Number_Too_Large with "Number is too large to print: " & English_Number'Image (Err);
      end if;
      if Beyond /= 0 then
         Cardinal_Aux (Result => Result, Number => Beyond, Period => Period + 1, Err => Err);
      end if;
      if Here /= 0 then
         if Beyond /= 0 then
            Append (Result, Space);
         end if;
         Small_Cardinal (Result, Here);
         if Period > 0 then
            Append (Result, Cardinal_Periods (Period));
         end if;
      end if;
   end Cardinal_Aux;


   --------------
   -- Cardinal --
   --------------

   function Cardinal
     (Number  : English_Number;
      Variant : Language_Variant := US_English) return String
   is
      Result : Unbounded_String;
   begin
      if Number < 0 then
         Append (Result, +"negative ");
         Cardinal_Aux (Result => Result, Number => -Number, Period => 0, Err => Number);
      elsif Number = 0 then
         Append (Result, +"zero");
      else
         Cardinal_Aux (Result => Result, Number => Number, Period => 0, Err => Number);
      end if;
      return To_String (Result);
   end Cardinal;


   -------------
   -- Ordinal --
   -------------

   function Ordinal
     (Number  : English_Number;
      Variant : Language_Variant := US_English) return String
   is
      Result  : Unbounded_String;
      Pos_Num : English_Number := abs Number;
      Top     : English_Number := Pos_Num / 100;
      Bot     : English_Number := Pos_Num mod 100;
      Tens    : Integer := Integer(Bot) / 10;
      Ones    : Integer := Integer(Bot) mod 10;
   begin
      if Number < 0 then
         Append (Result, +"negative ");
      end if;
      if Top /= 0 then
         Append (Result, Cardinal (Pos_Num - Bot));
      end if;
      if Top > 0 and Bot > 0 then
         Append (Result, Space);
      end if;
      if Bot = 12 then
         Append (Result, "twelfth");
      elsif Tens = 1 then
         Append (Result, (if Ones = 0 then Cardinal_Tens(1) else Cardinal_Teens (Ones)) & "th");
      elsif Tens = 0 and Ones > 0 then
         Append (Result, Ordinal_Ones (Ones));
      elsif Ones = 0 and Tens > 0 then
         Append (Result, Ordinal_Tens (Tens));
      elsif Bot > 0 then
         Append (Result, Cardinal_Tens (Tens) & "-" & (if Ones > 0 then Ordinal_Ones (Ones) else +""));
      elsif Pos_Num > 0 then
         Append (Result, "th");
      else
         Append (Result, "zeroth");
      end if;
      return To_String (Result);
   end Ordinal;


end English_Cardinals_Ordinals;
