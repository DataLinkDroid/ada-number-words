--  This package has been generated automatically by GNATtest.
--  Do not edit any part of it, see GNATtest documentation for more details.

--  begin read only
with AUnit.Test_Caller;
with Gnattest_Generated;

package body English_Cardinals_Ordinals.Test_Data.Tests.Suite is

   package Runner_1 is new AUnit.Test_Caller
     (GNATtest_Generated.GNATtest_Standard.English_Cardinals_Ordinals.Test_Data.Tests.Test);

   Result : aliased AUnit.Test_Suites.Test_Suite;

   Case_1_1_Test_Cardinal_79e0fa : aliased Runner_1.Test_Case;
   Case_2_1_Test_Ordinal_e7c678 : aliased Runner_1.Test_Case;

   function Suite return AUnit.Test_Suites.Access_Test_Suite is
   begin

      Runner_1.Create
        (Case_1_1_Test_Cardinal_79e0fa,
         "english_cardinals_ordinals.ads:52:4:",
         Test_Cardinal_79e0fa'Access);
      Runner_1.Create
        (Case_2_1_Test_Ordinal_e7c678,
         "english_cardinals_ordinals.ads:60:4:",
         Test_Ordinal_e7c678'Access);

      Result.Add_Test (Case_1_1_Test_Cardinal_79e0fa'Access);
      Result.Add_Test (Case_2_1_Test_Ordinal_e7c678'Access);

      return Result'Access;

   end Suite;

end English_Cardinals_Ordinals.Test_Data.Tests.Suite;
--  end read only
