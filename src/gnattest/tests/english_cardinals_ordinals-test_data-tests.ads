--  This package has been generated automatically by GNATtest.
--  Do not edit any part of it, see GNATtest documentation for more details.

--  begin read only
with Gnattest_Generated;

package English_Cardinals_Ordinals.Test_Data.Tests is

   type Test is new GNATtest_Generated.GNATtest_Standard.English_Cardinals_Ordinals.Test_Data.Test
   with null record;

   procedure Test_Cardinal_79e0fa (Gnattest_T : in out Test);
   --  english_cardinals_ordinals.ads:52:4:Cardinal

   procedure Test_Ordinal_e7c678 (Gnattest_T : in out Test);
   --  english_cardinals_ordinals.ads:60:4:Ordinal

end English_Cardinals_Ordinals.Test_Data.Tests;
--  end read only
